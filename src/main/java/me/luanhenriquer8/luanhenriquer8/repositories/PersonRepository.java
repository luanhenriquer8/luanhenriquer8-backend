package me.luanhenriquer8.luanhenriquer8.repositories;

import me.luanhenriquer8.luanhenriquer8.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {

}
