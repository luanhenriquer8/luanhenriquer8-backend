package me.luanhenriquer8.luanhenriquer8.services;

import me.luanhenriquer8.luanhenriquer8.model.Person;
import me.luanhenriquer8.luanhenriquer8.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public Iterable<Person> getAllPeople() {
        return personRepository.findAll();
    }

    public Person getPerson(Long id) {
        Person person = personRepository.findById(id).get();
        return person;
    }

    public void createPerson(String name, String email) {
        Person person = new Person();
        person.setName(name);
        person.setEmail(email);
        personRepository.save(person);
    }

    public void updatePerson(Long id, String name, String email) {
        Person person;
        person = personRepository.findById(id).get();
        person.setName(name);
        person.setEmail(email);
        personRepository.save(person);
    }

    public void deletePerson(Long id) {
        Person person;
        person = personRepository.findById(id).get();
        personRepository.delete(person);
    }
}
