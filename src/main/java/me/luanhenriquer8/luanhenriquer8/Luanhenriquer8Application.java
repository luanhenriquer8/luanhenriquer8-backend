package me.luanhenriquer8.luanhenriquer8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Luanhenriquer8Application {

	public static void main(String[] args) {
		SpringApplication.run(Luanhenriquer8Application.class, args);
	}
}
