package me.luanhenriquer8.luanhenriquer8.dto.parser;

import me.luanhenriquer8.luanhenriquer8.model.Person;

public class PersonParser {

    private Long id;
    private String name;

    public Person toDTO(Long id, String name) {
        Person person = new Person();
        person.setId(id);
        person.setName(name);
        return person;
    }
}
