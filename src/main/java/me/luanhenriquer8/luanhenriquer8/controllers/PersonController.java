package me.luanhenriquer8.luanhenriquer8.controllers;

import me.luanhenriquer8.luanhenriquer8.model.Person;
import me.luanhenriquer8.luanhenriquer8.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/person/")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private Person person;

    @ResponseBody
    @RequestMapping(path = "", method = RequestMethod.GET)
    public Iterable<Person> getAllPeople(){
        return personService.getAllPeople();
    }

    @ResponseBody
    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public Person getPerson(@PathVariable("id") Long id){
        person = personService.getPerson(id);
        return person;
    }


    @RequestMapping(path = "", method = RequestMethod.POST)
    public void createPerson(@RequestBody Person person){
        String name = person.getName();
        String email = person.getEmail();
        personService.createPerson(name, email);
    }

    @ResponseBody
    @RequestMapping(path = "", method = RequestMethod.PUT)
    public void updatePerson(@RequestBody Person person){
        Long id = person.getId();
        String name = person.getName();
        String email = person.getEmail();
        personService.updatePerson(id, name, email);
    }

    @ResponseBody
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void deletePerson(@PathVariable("id") Long id){
        personService.deletePerson(id);
    }




}
